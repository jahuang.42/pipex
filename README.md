# pipex

### Resources:  

[pipex tutorial — 42 project](https://csnotes.medium.com/pipex-tutorial-42-project-4469f5dd5901)  
[Simulating the pipe "|" operator in C](https://www.youtube.com/watch?v=6xbLgZpOBi8&list=RDCMUC6qj_bPq6tQ6hLwOBpBQ42Q&index=11)


### Other's piepx:  

https://github.com/LeoFu9487/pipex  
https://github.com/cclaude42/pipex  


### What I do to make this project:  

1. Learn about redirections. ( `<`, `>`, `<<`, `>>`, `2>`......)  


2. Learn how to use pipe(), fork()  
```
pipe(pid_t *pid_array)
fork(void) -> will return two pid_t
```


3. Learn how to use execve()  
```
execve(const char *path, char **const argv, char **const envp)
```


4. Overkill usage of hashtable to store environment variables.  
We only need PATH variable in this project.  
But I found some implementations of hashtable in other's minishell projects and do some research on it.  
Just try to implement this data structure here for practice reason and prepare for minishell later.  

5. Different shell behaviors on creating/opening outfile  
On Ubuntu linux system at 42 school with zsh. If the outfile is already exists, the pipe operation in the shell will quit with an error message: "zsh: file exists:".  But it's not the same on macos machines. So I reproduce different behaviors in my code.  

### How the program runs:  

- The program starts with the `main` function.  
	Check for the minimum requirements of arguments and send all the arguments to `pipex` function.  


- In `pipex`.  
	- Try to open "infile" with `ft_open_file` function. (Error exit if failed opening file).  
	- Use `dup2` to duplicate the "infile_fd" to "STDIN_FILENO" and close the "infile_fd".  
	- Start a loop to run the commands.  


- In `ft_run_pipe`.  
	- Get the path using `ft_get_path` and hashtable data structure.It can be optimize if I just store the hashtable at the begning of `pipex` instead of calling it every time when I run `ft_run_pipe`.  
	- Use `pipe` to create the PIPE.  
	- Use `fork` to fork out a child process.  
	- Run `ft_parent` and `ft_child` accordingly (by detecting the "fork_pid")  
	- `ft_child` will run the command with `execve` and write the result to "pipefd[1]"(write end)  
	- `ft_parent will wait for the child to finish and read the output of child process and send it to STDIN_FLENO.  


- `ft_run_last`  
	- When it comes to the last command we just open the "outfile" to write to and don't need pipe anymore.  
	- As always, `dup2` the "outfile_fd" to "STDOUT_FILENO", execute the command with `execve` and the result will be wirtten to the "outfile".  

- Error handling  
	- Try to reproduce the same behavior of "bash --posix".  
	- Error cases:  
		1. Error opening "infile" will print out warning message but won't exit with error code if it's the only problem.  
		2. When ever the executable path is missing an error message will be print out. And if it's the last commend, the program will exit with error code 127. Or else it will exit with code 0(since the last command is successfuly executed.)  
		3. If we run "unset PATH" to make $PATH become empty. It should show error message accordingly. (use "PATH=$(/bin/getconf PATH)" to restore PATH to default or just quit the terminal).
		4. When we dont have the permission to read the infile.
