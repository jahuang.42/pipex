make fclean
make bonus

echo "*****************************************************************"
echo "Tests: "

########################################################################

echo "*****************************************************************"
echo "wrong arg numbers"
./pipex_bonus
echo "exit code:" $?
echo " "

./pipex_bonus 1
echo "exit code:" $?
echo " "

./pipex_bonus 1 2 3 4 out 
echo "exit code:" $?
echo " "

./pipex_bonus 1 2 3 4 out
echo "exit code:" $?
echo " "

########################################################################

echo "*****************************************************************"
echo "ok"
./pipex_bonus Makefile cat wc outfile
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo "exit code:" $?
echo " "

echo "*****************************************************************"
< Makefile cat | wc > outfile
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo "bash exit code:" $?
echo " "

########################################################################

echo "*****************************************************************"
echo "error infile"
./pipex_bonus Ma cat wc outfile
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo "exit code:" $?
echo " "

< Ma cat | wc > outfile
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo "bash exit code:" $?
echo " "

########################################################################
echo "*****************************************************************"
echo "error cmd"
./pipex_bonus Makefile cat oo outfile
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo "exit code:" $?
echo " "
< Makefile cat | oo > outfile
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo "bash exit code:" $?
echo " "

echo "*****************************************************************"
./pipex_bonus Makefile kk oo outfile
echo "exit code: " $?
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo " "
< Makefile kk | oo > outfile
echo "exit code: " $?
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo " "

echo "*****************************************************************"
./pipex_bonus Makefile kk ls outfile
echo "exit code:" $?
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo " "
< Makefile kk | ls > outfile
echo "bash exit code:" $?
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo " "

########################################################################
echo "*****************************************************************"
echo "error infile + error cmd"

echo "*****************************************************************"
./pipex_bonus Ma cat oo outfile
echo "exit code:" $?
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo " "
< Ma cat | oo > outfile
echo "bash exit code:" $?
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo " "

echo "*****************************************************************"
./pipex_bonus Ma oo ls outfile
echo "exit code:" $?
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo " "
< Ma oo | ls > outfile
echo "bash exit code:" $?
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo " "

echo "*****************************************************************"
echo "./pipex_bonus Ma oo ls wc outfile"
./pipex_bonus Ma oo ls wc outfile
echo "exit code:" $?
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo " "
echo "< Ma oo | ls | wc > outfile"
< Ma oo | ls | wc > outfile
echo "bash exit code:" $?
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo " "

echo "*****************************************************************"
echo "./pipex_bonus Ma oo cat wc outfile"
./pipex_bonus Ma oo cat wc outfile
echo "exit code:" $?
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo " "
echo "< Ma oo | cat | wc > outfile"
< Ma oo | cat | wc > outfile
echo "bash exit code:" $?
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo " "

echo "*****************************************************************"
echo "./pipex_bonus Ma oo bb cat outfile"
./pipex_bonus Ma oo bb cat outfile
echo "exit code:" $?
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo " "
echo "< Ma oo | bb | cat > outfile"
< Ma oo | bb | cat  > outfile
echo "bash exit code:" $?
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo " "

echo "*****************************************************************"
echo "./pipex_bonus Makefile ls 'grep adfasdfasdfs' outfile"
./pipex_bonus Makefile ls "grep adfasdfasdfs" outfile
echo "exit code:" $?
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo " "
echo "< Makefile ls | grep aadsfasdasdfasdf > outfile"
< Makefile ls | grep aadsfasdasdfasdf > outfile
echo "bash exit code:" $?
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo " "

echo "*****************************************************************"
touch abc
chmod 000 abc
echo "no permissionn on infile"
./pipex_bonus abc cat wc outfile
echo "exit code:" $?
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo " "
< abc cat | wc > outfile
echo "bash exit code:" $?
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo " "
chmod 700 abc
rm abc

echo "*****************************************************************"
unset PATH
echo "PATH empty"
./pipex_bonus Makefile cat wc outfile
echo "exit code:" $?
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo " "
< Makefile cat | wc > outfile
echo "bash exit code:" $?
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo " "
PATH=$(/bin/getconf PATH)

rm out*
make clean
