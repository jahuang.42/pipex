make re
echo "Tests: "

########################################################################

echo "wrong arg numbers>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"

echo "test0~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
./pipex
echo "exit code:" $?
echo " "
echo "test1~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
./pipex 1
echo "exit code:" $?
echo " "
echo "test2~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
./pipex 1 2 3 4 5 
echo "exit code:" $?
echo " "
echo "test3~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
./pipex 1 2 3 4 5 
echo "exit code:" $?
echo " "

########################################################################
echo "ok >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
echo "./pipex Makefile cat wc outfile"
./pipex Makefile cat wc outfile
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo "exit code:" $?
echo " "
echo "< Makefile cat | wc > outfile"
< Makefile cat | wc > outfile
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo "bash exit code:" $?
echo " "

########################################################################
echo "error infile >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
echo "./pipex Ma cat wc outfile"
./pipex Ma ls wc outfile
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo "exit code:" $?
echo " "
echo "< Ma cat | wc > outfile"
< Ma ls | wc > outfile
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo "bash exit code:" $?
echo " "

########################################################################
echo "error 2nd cmd >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
echo "./pipex Makefile cat oo outfile"
./pipex Makefile cat oo outfile
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo "exit code:" $?
echo " "
echo "< Makefile cat | oo > outfile"
< Makefile cat | oo > outfile
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo "bash exit code:" $?
echo " "

########################################################################
echo "error 2 cmds  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
echo "./pipex Makefile kk oo outfile"
./pipex Makefile kk oo outfile
echo "exit code: " $?
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo " "
echo "< Makefile kk | oo > outfile"
< Makefile kk | oo > outfile
echo "exit code: " $?
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo " "

########################################################################
echo "error 1st cmd >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
echo "./pipex Makefile kk ls outfile"
./pipex Makefile kk ls outfile
echo "exit code:" $?
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo " "
echo "< Makefile kk | ls > outfile"
< Makefile kk | ls > outfile
echo "bash exit code:" $?
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo " "

########################################################################
echo "error infile + error cmd >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"

echo "./pipex Ma cat oo outfile"
./pipex Ma cat oo outfile
echo "exit code:" $?
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo " "
echo "< Ma cat | oo > outfile"
< Ma cat | oo > outfile
echo "bash exit code:" $?
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo " "

echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"

echo "./pipex Ma oo ls outfile"
./pipex Ma oo ls outfile
echo "exit code:" $?
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo " "
echo "< Ma oo | ls > outfile"
< Ma oo | ls > outfile
echo "bash exit code:" $?
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo " "

echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"

echo "./pipex Ma ls wc outfile"
./pipex Ma ls wc outfile
echo "exit code:" $?
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo " "
echo "< Ma ls | wc > outfile"
< Ma ls | wc > outfile
echo "bash exit code:" $?
echo "------------------outfile----------------"
cat outfile
echo "-----------------------------------------"
echo " "

rm out*
make clean
