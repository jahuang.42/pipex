/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipex_bonus.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/22 11:17:29 by jahuang           #+#    #+#             */
/*   Updated: 2021/12/01 13:00:44 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PIPEX_BONUS_H
# define PIPEX_BONUS_H

# include <fcntl.h>
# include <unistd.h>
# include <stdio.h>
# include <sys/stat.h>
# include <sys/types.h>
# include <sys/wait.h>

# include "../libft/libft.h"
# include "../libft/get_next_line/get_next_line.h"
# include "./hashtable.h"

int			ft_index_of(char *str, char c);
int			ft_arraylen(char **str);
int			ft_next_prime(int nbr);
t_hashtable	*ft_get_env(char **env);
char		*ft_get_path(char **env, char *cmd, int av_index, int infile_fd);
int			ft_open_file(char *filename, int io);
void		ft_free_char_array(char **char_array);

#endif
