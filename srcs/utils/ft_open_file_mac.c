/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_open_file_mac.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/30 16:55:18 by jahuang           #+#    #+#             */
/*   Updated: 2021/11/29 11:25:31 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifdef BONUS
# include "pipex_bonus.h"
#else
# include "pipex.h"
#endif

int	ft_open_infile(char *filename)
{
	int	file_fd;

	if (access(filename, F_OK) == 0)
		file_fd = open(filename, O_RDONLY);
	else
	{
		ft_putstr_fd("pipex: no such file or directory: ", 2);
		ft_putstr_fd(filename, 2);
		ft_putstr_fd("\n", 2);
		exit (1);
	}
	return (file_fd);
}

int	ft_open_file(char *filename, int io)
{
	int	file_fd;

	if (io == 0)
		file_fd = ft_open_infile(filename);
	if (io == 1)
	{
		file_fd = open(filename, O_WRONLY | O_CREAT | O_TRUNC,
				S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH);
		if (file_fd < 0)
		{
			ft_putstr_fd("pipex: error creating outfile: ", 2);
			ft_putstr_fd(filename, 2);
			ft_putstr_fd("\n", 2);
			return (file_fd);
		}
	}
	return (file_fd);
}
