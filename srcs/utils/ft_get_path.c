/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_path.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/31 15:30:55 by jahuang           #+#    #+#             */
/*   Updated: 2021/12/08 13:15:12 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifdef BONUS
# include "pipex_bonus.h"
#else
# include "pipex.h"
#endif

void	ft_print_error(int error_code, char *cmd, int absolute)
{
	if (error_code == 1)
	{
		ft_putstr_fd("pipex: command not found: ", 2);
		if (ft_strncmp(cmd, "/", 1) == 0 && !absolute)
			cmd++;
		ft_putstr_fd(cmd, 2);
		ft_putstr_fd("\n", 2);
	}
	if (error_code == 2)
	{
		ft_putstr_fd("pipex: permission denied: ", 2);
		if (ft_strncmp(cmd, "/", 1) == 0 && !absolute)
			cmd++;
		ft_putstr_fd(cmd, 2);
		ft_putstr_fd("\n", 2);
	}
}

int	ft_check_access(char *path)
{
	int	result_code;

	result_code = 0;
	if (access(path, F_OK) == 0)
	{
		if (access(path, X_OK) == 0)
		{
			result_code = 0;
		}
		else
			result_code = 2;
	}
	else if (result_code == 0)
		result_code = 1;
	return (result_code);
}

int	ft_find_cmd_in_paths(char *cmd, char **env_paths, char **path_addr)
{
	int		index;
	int		check_code;
	char	*cmd_join;

	index = 0;
	check_code = 0;
	cmd_join = ft_strjoin("/", cmd);
	while (env_paths[index])
	{
		*path_addr = ft_strjoin(env_paths[index], cmd_join);
		if (ft_check_access(*path_addr) == 1 && check_code == 2)
			check_code = 2;
		else
			check_code = ft_check_access(*path_addr);
		if (check_code == 0)
		{
			free(cmd_join);
			return (check_code);
		}
		free(*path_addr);
		index++;
	}
	free(cmd_join);
	return (check_code);
}

char	*ft_get_exec(char **env_paths, char *cmd, int av_index, int infile_fd)
{
	int		check_code;
	char	*path;

	if (!cmd || !env_paths[0])
		check_code = 1;
	else
	{
		if (ft_strncmp(cmd, "/", 1) == 0)
		{
			check_code = ft_check_access(cmd);
			if (check_code == 0)
				return (ft_strdup(cmd));
		}
		else
			check_code = ft_find_cmd_in_paths(cmd, env_paths, &path);
	}
	if (check_code == 0)
		return (path);
	if (av_index != 2 || infile_fd >= 0)
		ft_print_error(check_code, cmd, 0);
	return (NULL);
}

char	*ft_get_path(char **env, char *cmd, int av_index, int infile_fd)
{
	t_hashtable	*env_hashtable;
	char		**env_paths;
	char		*path;
	char		*path_value;

	env_hashtable = ft_get_env(env);
	path_value = ft_get_value(env_hashtable, "PATH");
	if (!path_value)
	{
		ft_free_hashtable(env_hashtable);
		if (av_index != 2 || infile_fd >= 0)
			ft_print_error(1, cmd, 0);
		return (NULL);
	}
	env_paths = ft_split(path_value, ':');
	path = ft_get_exec(env_paths, cmd, av_index, infile_fd);
	ft_free_char_array(env_paths);
	ft_free_hashtable(env_hashtable);
	return (path);
}
