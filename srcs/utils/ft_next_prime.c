/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_next_prime.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/26 11:57:33 by jahuang           #+#    #+#             */
/*   Updated: 2021/11/22 11:23:42 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifdef BONUS
# include "pipex_bonus.h"
#else
# include "pipex.h"
#endif

int	ft_isprime(int nbr)
{
	int	mod;

	mod = 2;
	while (mod < nbr / 2)
	{
		if (nbr % mod == 0)
			return (0);
		mod++;
	}
	return (1);
}

int	ft_next_prime(int nbr)
{
	int	result;

	result = nbr;
	while (ft_isprime(result) == 0)
		result++;
	return (result);
}
