/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_env.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/26 11:56:45 by jahuang           #+#    #+#             */
/*   Updated: 2021/11/22 11:22:57 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifdef BONUS
# include "pipex_bonus.h"
#else
# include "pipex.h"
#endif

void	ft_env_to_hashtable(char **env, t_hashtable *hashtable, int array_len)
{
	int			env_index;
	int			char_index;
	int			hash_index;
	char		*key;
	char		*value;

	env_index = 0;
	while (env[env_index])
	{
		char_index = ft_index_of(env[env_index], '=');
		key = ft_substr(env[env_index], 0, char_index);
		value = ft_substr(
				env[env_index],
				char_index + 1,
				ft_strlen(env[env_index]) - char_index - 1);
		hash_index = ft_monkey_hash(key, array_len);
		while (hashtable->element_array[hash_index])
		{
			hash_index = (hash_index + 1) % array_len;
		}
		hashtable->element_array[hash_index] = ft_create_element(key, value);
		free(key);
		free(value);
		env_index++;
	}
}

t_hashtable	*ft_get_env(char **env)
{
	t_hashtable	*hashtable;
	int			array_len;

	if (ft_arraylen(env) == 0)
	{
		ft_putstr_fd("No environment variables.\n", 2);
		exit(1);
	}
	array_len = ft_next_prime(ft_arraylen(env) * 2);
	hashtable = ft_create_hashtable(array_len);
	ft_env_to_hashtable(env, hashtable, array_len);
	return (hashtable);
}
