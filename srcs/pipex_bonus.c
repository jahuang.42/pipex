/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipex_bonus.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/31 16:02:54 by jahuang           #+#    #+#             */
/*   Updated: 2021/12/08 14:21:20 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex_bonus.h"

void	ft_tmp_redirect(int *pipefd, int *infile_fd)
{
	int	tmp_fd;

	tmp_fd = open("tmp", O_WRONLY | O_CREAT, S_IRUSR | S_IRGRP | S_IROTH);
	dup2(tmp_fd, pipefd[1]);
	close(tmp_fd);
	unlink("tmp");
	dup2(pipefd[1], STDOUT_FILENO);
	close(pipefd[1]);
	dup2(pipefd[0], STDIN_FILENO);
	close(pipefd[0]);
	*infile_fd = 0;
}

void	ft_run_fork(int *pipefd, char *path, char **arg, char **env)
{
	int		fork_pid;

	fork_pid = fork();
	if (fork_pid)
	{
		close(pipefd[1]);
		dup2(pipefd[0], STDIN_FILENO);
		close(pipefd[0]);
		waitpid(fork_pid, NULL, 0);
	}
	else
	{
		close(pipefd[0]);
		dup2(pipefd[1], STDOUT_FILENO);
		close(pipefd[1]);
		execve(path, arg, env);
	}
}

void	ft_run_pipe(char *cmd, char **env, int av_index, int *infile_fd)
{
	char	*path;
	char	**arg;
	int		pipefd[2];

	arg = ft_split(cmd, ' ');
	if (!arg)
		return ;
	path = ft_get_path(env, arg[0], av_index, *infile_fd);
	pipe(pipefd);
	if (path && *infile_fd >= 0)
		ft_run_fork(pipefd, path, arg, env);
	else
		ft_tmp_redirect(pipefd, infile_fd);
	ft_free_char_array(arg);
	if (path)
		free(path);
}

void	ft_run_last(int av_index, char **av, char **env, int infile_fd)
{
	char	**last_arg;
	char	*path;
	int		outfile_fd;

	last_arg = ft_split(av[av_index], ' ');
	path = ft_get_path(env, last_arg[0], av_index, infile_fd);
	outfile_fd = ft_open_file(av[av_index + 1], 1);
	if (outfile_fd < 0)
	{
		ft_free_char_array(last_arg);
		free(path);
		exit(1);
	}
	dup2(outfile_fd, STDOUT_FILENO);
	if (!path)
	{
		ft_free_char_array(last_arg);
		close(outfile_fd);
		exit(127);
	}
	execve(path, last_arg, env);
	close(outfile_fd);
	ft_free_char_array(last_arg);
	free(path);
}

int	main(int ac, char **av, char **env)
{
	int	infile_fd;
	int	av_index;

	if (ac < 5)
	{
		ft_putstr_fd("pipex: please provide at least 4 arguments.\n", 2);
		exit(1);
	}
	infile_fd = ft_open_file(av[1], 0);
	if (infile_fd > 0)
	{
		dup2(infile_fd, STDIN_FILENO);
		close(infile_fd);
	}
	av_index = 2;
	while (av_index < ac - 2)
	{
		ft_run_pipe(av[av_index], env, av_index, &infile_fd);
		av_index++;
	}
	ft_run_last(av_index, av, env, infile_fd);
	return (0);
}
