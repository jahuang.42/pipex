/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_create_hashtable.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/31 15:47:54 by jahuang           #+#    #+#             */
/*   Updated: 2021/11/22 11:24:56 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifdef BONUS
# include "pipex_bonus.h"
#else
# include "pipex.h"
#endif

t_hashtable	*ft_create_hashtable(int length)
{
	int			index;
	t_hashtable	*new_hashtable;

	index = 0;
	new_hashtable = (t_hashtable *)malloc(sizeof(t_hashtable) * 1);
	new_hashtable->length = length;
	new_hashtable->element_array = malloc(sizeof(t_element) * length);
	while (index < length)
	{
		new_hashtable->element_array[index] = NULL;
		index++;
	}
	return (new_hashtable);
}
