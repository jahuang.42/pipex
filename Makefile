# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jahuang <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2021/11/19 17:23:43 by jahuang           #+#    #+#              #
#    Updated: 2021/12/03 13:55:57 by jahuang          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME			=	pipex
BONUS			=	pipex_bonus

CC				=	gcc
CFLAGS			=	-Wall -Wextra -Werror
FSAN			=	-fsanitize=address
RM				=	rm -rf
BONUS_FLAG		=	-D BONUS
OS				=	$(shell uname)

LEAK_CHECK		=	pipex_leak_check
B_LEAK_CHECK	=	pipex_bonus_leak_check

LIBFT_A			=	libft.a
LIBFT_DIR		=	libft

SRCS_DIR		=	srcs
INCS_DIR		=	incs

INCS			=	pipex.h

SRCS			=	pipex.c \
					hashtable/ft_create_hashtable.c \
					hashtable/ft_create_element.c \
					hashtable/ft_print_hashtable.c \
					hashtable/ft_monkey_hash.c \
					hashtable/ft_get_value.c \
					hashtable/ft_free_hashtable.c \
					utils/ft_index_of.c \
					utils/ft_arraylen.c \
					utils/ft_next_prime.c \
					utils/ft_get_env.c \
					utils/ft_get_path.c \
					utils/ft_free_char_array.c \
					utils/ft_open_file.c \

BONUS_SRCS		=	pipex_bonus.c \
					hashtable/ft_create_hashtable.c \
					hashtable/ft_create_element.c \
					hashtable/ft_print_hashtable.c \
					hashtable/ft_monkey_hash.c \
					hashtable/ft_get_value.c \
					hashtable/ft_free_hashtable.c \
					utils/ft_index_of.c \
					utils/ft_arraylen.c \
					utils/ft_next_prime.c \
					utils/ft_get_env.c \
					utils/ft_get_path.c \
					utils/ft_free_char_array.c \
					utils/ft_open_file.c \

OBJS			=	$(addprefix $(SRCS_DIR)/,$(SRCS:.c=.o))

BONUS_OBJS		=	$(addprefix $(SRCS_DIR)/,$(BONUS_SRCS:.c=.o))

%.o				:	%.c
					$(CC) $(CFLAGS) -I $(INCS_DIR) -c $< -o $@

$(NAME)			:	$(OBJS) $(LIBFT_A)
					$(CC) -o $@ $(OBJS) $(LIBFT_DIR)/$(LIBFT_A)

$(BONUS)		:	INCS = pipex_bonus.h

$(BONUS)		:	$(BONUS_OBJS) $(LIBFT_A)
					$(CC) -o $@ $(BONUS_OBJS) $(LIBFT_DIR)/$(LIBFT_A) $(BONUS_FLAG)

$(LEAK_CHECK)	:	$(OBJS) $(LIBFT_A)
					$(CC) $(FSAN) -o $@ $(OBJS) $(LIBFT_DIR)/$(LIBFT_A)

$(B_LEAK_CHECK)	:	$(BONUS_OBJS) $(LIBFT_A)
					$(CC) $(FSAN) -o $@ $(BONUS_OBJS) $(LIBFT_DIR)/$(LIBFT_A)

$(LIBFT_A)		:
					@make -C $(LIBFT_DIR)

all				:	$(NAME)

bonus			:	$(BONUS)

leak_check		:	$(LEAK_CHECK)

b_leak_check	:	$(B_LEAK_CHECK)

clean			:
					@$(RM) $(OBJS)
					@$(RM) $(BONUS_OBJS)
					@make clean -C $(LIBFT_DIR)

fclean			:	clean
					@$(RM) $(NAME) $(LEAK_CHECK)
					@$(RM) $(BONUS) $(B_LEAK_CHECK)
					@make fclean -C $(LIBFT_DIR)

re				:	fclean all

.PHONY : all clean fclean re
